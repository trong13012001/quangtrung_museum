<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoricalFigures extends Model
{
    use HasFactory;

    protected $table = 'historical_figures';

    protected $fillable = ['id', 'vn_historical_figures_name','en_historical_figures_name', 'vn_historical_figures_story','en_historical_figures_story', 'historical_figures_image', 'historical_figures_map'];
}

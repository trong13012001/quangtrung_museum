<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\HistoricalFigures;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class HistoricalFiguresController extends Controller
{

    
    /**
     * @OA\GET(
     *     path="/api/historicalfigures/vn/{id}",
     *     summary="Query for historical figures Vietnamese",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Historical Figures ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(response="404", description="Nhân vật lịch sử không tìm thấy")
     * )
     */
    public function queryVNHistoricalFigures($id)
{
    // Find the historical figure by its ID
    $record = HistoricalFigures::find($id);

    // Check if the record exists
    if (!$record) {
        return response()->json(['message' => 'Nhân vật lịch sử không tìm thấy'], 404);
    }

    // Extract the data from the record
    $data = [
        'historicalFiguresId' => $record->id,
        'historicalFiguresName' => $record->vn_historical_figures_name,
        'historicalFiguresStory' => $record->vn_historical_figures_story,
        'historicalFiguresImage' => $record->historical_figures_image,
        'historicalFiguresMap' => $record->historical_figures_map,
    ];

    // Return the data as a JSON response
    return response()->json($data, 200);
}

 /**
     * @OA\GET(
     *     path="/api/historicalfigures/en/{id}",
     *     summary="Query for historical figures English",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Historical Figures ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(response="404", description="Historical Figures not found")
     * )
     */

    public function queryENHistoricalFigures($id)
    {
        $record = HistoricalFigures::find($id);

    // Check if the record exists
    if (!$record) {
        return response()->json(['message' => 'Historical Figures not found'], 404);
    }

    // Extract the data from the record
    $data = [
        'historicalFiguresId' => $record->id,
        'historicalFiguresName' => $record->en_historical_figures_name,
        'historicalFiguresStory' => $record->en_historical_figures_story,
        'historicalFiguresImage' => $record->historical_figures_image,
        'historicalFiguresMap' => $record->historical_figures_map,
    ];

    // Return the data as a JSON response
    return response()->json($data, 200);
    }



        /**
         * @OA\POST(
         *     path="/api/upload-image/{id}",
         *     summary="Upload Image Historical Figure",
         *     @OA\Parameter(
         *         name="id",
         *         in="path",
         *         description="ID Historical Figure",
         *         required=true,
         *         @OA\Schema(type="string")
         *     ),
        *     @OA\RequestBody(
        *         @OA\MediaType(
        *             mediaType="multipart/form-data",
   
        *                     @OA\Schema(
        *                         @OA\Property(
        *                             description="Item image",
        *                             property="image",
        *                             type="string", format="binary"
        *                         )
        *                     )
        *         )
        *     ),
         *     @OA\Response(response="400", description="Failed to upload image")
         * )
         */
    public function uploadImage(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // Adjust as needed
        ]);
    
        $uploadedImage = $request->file('image');
    
        if ($uploadedImage) {
            $base64Image = base64_encode(file_get_contents($uploadedImage->path()));
    
            // Find the historical figure by its ID
            $historicalFigure = HistoricalFigures::find($id);
    
            if (!$historicalFigure) {
                return response()->json(['message' => 'Historical figure not found'], 404);
            }
    
            // Update the historical_figures_image column with the base64 image data
            $historicalFigure->historical_figures_image = $base64Image;
            $historicalFigure->save();
    
            return response()->json(['message' => 'Image uploaded successfully', 'image' => $base64Image], 201);
        } else {
            return response()->json(['message' => 'Failed to upload image'], 400);
        }
    }
            /**
         * @OA\POST(
         *     path="/api/upload-map/{id}",
         *     summary="Upload Map Historical Figure",
         *     @OA\Parameter(
         *         name="id",
         *         in="path",
         *         description="ID Historical Figure",
         *         required=true,
         *         @OA\Schema(type="string")
         *     ),
        *     @OA\RequestBody(
        *         @OA\MediaType(
        *             mediaType="multipart/form-data",
   
        *                     @OA\Schema(
        *                         @OA\Property(
        *                             description="Item image",
        *                             property="image",
        *                             type="string", format="binary"
        *                         )
        *                     )
        *         )
        *     ),
         *     @OA\Response(response="400", description="Failed to upload image")
         * )
         */
    public function uploadMap(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // Adjust as needed
        ]);
    
        $uploadedImage = $request->file('image');
    
        if ($uploadedImage) {
            $base64Image = base64_encode(file_get_contents($uploadedImage->path()));
    
            // Find the historical figure by its ID
            $historicalFigure = HistoricalFigures::find($id);
    
            if (!$historicalFigure) {
                return response()->json(['message' => 'Historical figure not found'], 404);
            }
    
            // Update the historical_figures_image column with the base64 image data
            $historicalFigure->historical_figures_map = $base64Image;
            $historicalFigure->save();
    
            return response()->json(['message' => 'Map uploaded successfully', 'image' => $base64Image], 201);
        } else {
            return response()->json(['message' => 'Failed to upload image'], 400);
        }
    }


    /**
     * @OA\Post(
     *     path="/api/historicalfigures/add",
     *     summary="Add Historical Figure",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="vn_historical_figures_name",
     *                     type="string",
     *                     maxLength=255,
     *                     description="Vietnamese Historical Figure Name"
     *                 ),
     *                 @OA\Property(
     *                     property="vn_historical_figures_story",
     *                     type="string",
     *                     description="Vietnamese Historical Figure Story"
     *                 ),
     *                 @OA\Property(
     *                     property="en_historical_figures_name",
     *                     type="string",
     *                     maxLength=255,
     *                     description="English Historical Figure Name"
     *                 ),
     *                 @OA\Property(
     *                     property="en_historical_figures_story",
     *                     type="string",
     *                     description="English Historical Figure Story"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Historical Figure added successfully",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="success",
     *                 type="string",
     *                 example="Add Historical Figure Successfully"
     *             ),
     *             @OA\Property(
     *                 property="user",
     *                 type="object",
     *                 description="The created Historical Figure object",
     *                 @OA\Property(
     *                     property="vnHistoricalFigureName",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="vnHistoricalFigureStory",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="enHistoricalFigureName",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="enHistoricalFigureStory",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Bad Request, validation failed",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="errors",
     *                 type="object",
     *                 description="Validation errors",
     *                 example={
     *                     "vnHistoricalFigureName": {"The vnHistoricalFigureName field is required."},
     *                     "enHistoricalFigureName": {"The enHistoricalFigureName field is required."}
     *                 }
     *             )
     *         )
     *     )
     * )
     */
    public function addHistoricalFigure(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'vn_historical_figures_name'     => 'string|max:255',
            'vn_historical_figures_story' => 'string',
            'en_historical_figures_name'    => 'string|max:255',
            'en_historical_figures_story' => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $user = HistoricalFigures::create([
            'vn_historical_figures_name'     => $request->vn_historical_figures_name,
            'vn_historical_figures_story' => $request->vn_historical_figures_story,
            'en_historical_figures_name'    => $request->en_historical_figures_name,
            'en_historical_figures_story' => $request->en_historical_figures_story,
        ]);


        return response()->json([
            'success'       => 'Add Historical Figures Successfully',
        ]);
    }
    
    
    
}

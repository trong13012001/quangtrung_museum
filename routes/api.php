<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\UploadController;
use App\Http\Controllers\Api\ArtifactController;
use App\Http\Controllers\Api\HistoricalFiguresController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('historicalfigures/vn/{id}', [HistoricalFiguresController::class, 'queryVNHistoricalFigures']);
Route::get('historicalfigures/en/{id}', [HistoricalFiguresController::class, 'queryENHistoricalFigures']);
Route::post('/upload-image/{id}', [HistoricalFiguresController::class, 'uploadImage']);
Route::post('/upload-map/{id}', [HistoricalFiguresController::class, 'uploadMap']);
Route::post('/historicalfigures/add', [HistoricalFiguresController::class, 'addHistoricalFigure']);
